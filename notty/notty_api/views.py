import pytz
from main.settings import TIME_ZONE
import datetime

from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from models import Color, Tag, Note, Category, UserProfile
from serializers import ColorSerializer, TagsSerializer, NoteSerializer, \
    CategorySerializer, UserProfileSerializer


class ColorViewSet(viewsets.ModelViewSet):
    """
    ViewSet for note colors
    """
    permission_classes = (IsAuthenticated, )
    serializer_class = ColorSerializer

    def list(self, request, *args, **kwargs):
        """ Returns list of colors"""
        queryset = Color.objects.all()
        serializer = ColorSerializer(queryset, many=True)

        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        """Returns color by his name"""
        colorname = kwargs['color_name']
        queryset = Color.objects.get(color_name=colorname)
        serializer = ColorSerializer(queryset)

        return Response(serializer.data)


class TagViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = TagsSerializer
    queryset = Tag.objects.all()

    def retrieve(self, request, *args, **kwargs):
        tagname = kwargs['tag_name']
        queryset = Tag.objects.get(tag_name=tagname)
        serializer = TagsSerializer(queryset)

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        tagname = kwargs['tag_name']
        queryset = Tag.objects.get(tag_name=tagname)
        queryset.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()

    def retrieve(self, request, *args, **kwargs):
        catname = kwargs['category_name']
        queryset = Category.objects.get(title=catname)
        serializer = CategorySerializer(queryset)

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        catname = kwargs['category_name']
        queryset = Category.objects.get(title=catname)
        queryset.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class NoteViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = NoteSerializer
    queryset = Note.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = Note.objects.filter(notes__user__id=request.user.id)
        serializer = NoteSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        note_id = kwargs['pk']
        queryset = Note.objects.filter(notes__user__id=request.user.id)

        if queryset.filter(id=note_id).exists():
            serializer = NoteSerializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def create(self, request, *args, **kwargs):

        time_now = datetime.datetime.now(pytz.timezone(TIME_ZONE))

        color = Color.objects.get(color_hex=request.data['color'])

        try:
            category = Category.objects.get(
                title=request.data['categories.title'])
        except:
            category = None

        note = Note(
            title=request.data['title'],
            content=request.data['content'],
            date=time_now,
            color=color,
            categories=category,
        )

        if 'picture' in request.FILES:
            image = request.FILES['picture']
            note.picture.save(str(image), image)

        note.save()
        request.user.userprofile.notes.add(note)

        return Response(status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        key = kwargs['pk']
        note = Note.objects.get(id=key)
        note.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs): # didn't work
        pass
        # key = kwargs['pk']
        # note = Note.objects.get(id=key)
        #
        # time_now = datetime.datetime.now(pytz.timezone(TIME_ZONE))
        # try:
        #     color = Color.objects.get(color_hex=request.data['color'])
        # except:
        #     color = Color.objects.get(color_name=note.color)
        # try:
        #     category = Category.objects.get(
        #         title=request.data['categories.title'])
        # except:
        #     category = None
        #
        # note.title = request.data['title']
        # note.content = request.data['content'],
        # note.date = time_now,
        # note.color = color
        # note.categories = category
        # note.save()
        # serializer = NoteSerializer(note, data=request.data, partial=True)
        #
        # return Response(serializer.initial_data)


class UserViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer()

    def retrieve(self, request, *args, **kwargs):
        user = UserProfile.objects.get(id=request.user.userprofile.id)
        serializer = UserProfileSerializer(user)

        return Response(serializer.data)

