from django.conf.urls import url
import views


urlpatterns = [
    url(r'^colors/$', views.ColorViewSet.as_view({'get': 'list'})),
    url(r'^colors/(?P<color_name>[0-9a-zA-Z_-]+)/$',
        views.ColorViewSet.as_view({'get': 'retrieve'})),

    url(r'^tags/$', views.TagViewSet.as_view({'get': 'list',
                                              'post': 'create'})),

    url(r'^tags/(?P<tag_name>[0-9a-zA-Z_-]+)$',
        views.TagViewSet.as_view({'get': 'retrieve',
                                  'delete': 'destroy'})),


    url(r'notes/$', views.NoteViewSet.as_view({'get': 'list',
                                               'post': 'create'})),
    url(r'notes/(?P<pk>\d+)/$', views.NoteViewSet.as_view(
        {'get': 'retrieve',
         'delete': 'destroy',
         'patch': 'update'})),

    url(r'categories/$', views.CategoryViewSet.as_view({'get': 'list',
                                                        'post': 'create'})),

    url(r'categories/(?P<category_name>[0-9a-zA-Z_-]+)/$',
        views.CategoryViewSet.as_view({'get': 'retrieve',
                                       'delete': 'destroy'})),

    url(r'profile/$', views.UserViewSet.as_view({'get': 'retrieve',
                                                 }))


]
