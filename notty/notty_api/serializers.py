from django.contrib.auth.models import User
from rest_framework import serializers

from .models import UserProfile, Tag, Note, Category, Color, Media


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email',)


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = ('color_name', 'color_hex',)


class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('tag_name',)


class CategorySerializer(serializers.ModelSerializer):
    title = serializers.CharField(allow_blank=True)

    class Meta:
        model = Category
        fields = ('parent', 'title',)


class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media
        fields = ('id', 'media_item', )


class ColorRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        return self.queryset.get(color_name=value).color_hex

    def to_internal_value(self, data):
        print data


class NoteSerializer(serializers.ModelSerializer):
    color = ColorRelatedField(queryset=Color.objects.all(), allow_null=True)

    tags = TagsSerializer(many=True, read_only=False)

    categories = CategorySerializer(read_only=False, allow_null=True)

    medias = MediaSerializer(many=True, allow_null=True)

    picture = serializers.ImageField(allow_null=True)

    class Meta:
        model = Note
        fields = ('id', 'title', 'content', 'picture', 'color', 'tags',
                  'categories', 'medias')
        read_only_fields = ('id',)


class UserProfileSerializer(serializers.ModelSerializer):

    notes = NoteSerializer(many=True)

    class Meta:
        model = UserProfile
        fields = ('user', 'avatar', 'notes')
