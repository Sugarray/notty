# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0016_note_category'),
    ]

    operations = [
        migrations.RenameField(
            model_name='note',
            old_name='category',
            new_name='categories',
        ),
    ]
