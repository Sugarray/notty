# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0026_auto_20150623_1039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='picture',
            field=models.ImageField(upload_to=b'images/', blank=True),
        ),
    ]
