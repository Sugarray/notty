# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0020_auto_20150622_1907'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='medias',
            field=models.ManyToManyField(related_name='media_files', to='notty_api.Media'),
        ),
    ]
