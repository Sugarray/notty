# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='picture',
            field=models.ImageField(null=True, upload_to=b'media/images', blank=True),
        ),
    ]
