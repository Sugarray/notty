# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0007_auto_20150619_1227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='media',
            name='media_item',
            field=models.FileField(upload_to=b'files/'),
        ),
    ]
