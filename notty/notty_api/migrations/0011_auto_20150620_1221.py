# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0010_auto_20150620_1220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='media',
            name='media_item',
            field=models.FileField(null=True, upload_to=b'files/', blank=True),
        ),
    ]
