# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import notty_api.models


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0021_note_medias'),
    ]

    operations = [
        migrations.AlterField(
            model_name='color',
            name='color_hex',
            field=notty_api.models.RGBColorField(),
        ),
        migrations.AlterField(
            model_name='note',
            name='categories',
            field=models.ForeignKey(related_name='categories', to='notty_api.Category', null=True),
        ),
    ]
