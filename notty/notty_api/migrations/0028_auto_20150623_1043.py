# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0027_auto_20150623_1041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='categories',
            field=models.ForeignKey(related_name='categories', blank=True, to='notty_api.Category', null=True),
        ),
        migrations.AlterField(
            model_name='note',
            name='picture',
            field=models.ImageField(null=True, upload_to=b'images/', blank=True),
        ),
    ]
