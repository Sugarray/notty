# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0019_auto_20150622_0314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='parent',
            field=mptt.fields.TreeForeignKey(related_name='child', blank=True, to='notty_api.Category', null=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(default=b'title', unique=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='note',
            name='categories',
            field=models.ForeignKey(related_name='categories', blank=True, to='notty_api.Category', null=True),
        ),
    ]
