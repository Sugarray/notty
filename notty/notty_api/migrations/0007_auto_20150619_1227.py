# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0006_auto_20150618_0949'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='picture',
            field=models.ImageField(null=True, upload_to=b'images/', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='avatar',
            field=models.ImageField(null=True, upload_to=b'avatars/', blank=True),
        ),
    ]
