# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0023_remove_note_categories'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='categories',
            field=models.ForeignKey(related_name='categories', to='notty_api.Category', null=True),
        ),
    ]
