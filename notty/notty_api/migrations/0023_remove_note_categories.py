# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0022_auto_20150623_0053'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='categories',
        ),
    ]
