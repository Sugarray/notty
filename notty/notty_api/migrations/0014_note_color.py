# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0013_remove_note_tags'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='color',
            field=models.ForeignKey(related_name='color', default=1, to='notty_api.Color'),
        ),
    ]
