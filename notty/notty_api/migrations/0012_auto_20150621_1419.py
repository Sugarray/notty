# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0011_auto_20150620_1221'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='category',
        ),
        migrations.RemoveField(
            model_name='note',
            name='color',
        ),
        migrations.RemoveField(
            model_name='note',
            name='medias',
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='notes',
            field=models.ManyToManyField(related_name='notes', to='notty_api.Note'),
        ),
    ]
