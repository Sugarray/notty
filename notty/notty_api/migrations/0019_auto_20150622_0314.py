# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0018_auto_20150622_0306'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.RemoveField(
            model_name='note',
            name='categories',
        ),
        migrations.AddField(
            model_name='note',
            name='categories',
            field=models.ForeignKey(related_name='categories', to='notty_api.Category', null=True),
        ),
    ]
