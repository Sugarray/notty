# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0003_userprofile'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='category',
            field=models.ManyToManyField(to='notty_api.Category'),
        ),
    ]
