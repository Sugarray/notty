# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0008_auto_20150619_1304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='color',
        ),
        migrations.AddField(
            model_name='note',
            name='color',
            field=models.ForeignKey(default=1, to='notty_api.Color'),
        ),
    ]
