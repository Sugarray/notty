# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0025_auto_20150623_0105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='tags',
            field=models.ManyToManyField(related_name='tags', null=True, to='notty_api.Tag', blank=True),
        ),
    ]
