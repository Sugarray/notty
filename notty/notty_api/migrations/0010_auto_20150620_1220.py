# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notty_api', '0009_auto_20150619_1514'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='medias',
            field=models.ManyToManyField(related_name='media_files', to='notty_api.Media'),
        ),
        migrations.AlterField(
            model_name='note',
            name='category',
            field=models.ManyToManyField(related_name='categories', to='notty_api.Category'),
        ),
        migrations.AlterField(
            model_name='note',
            name='color',
            field=models.ForeignKey(related_name='color', default=1, to='notty_api.Color'),
        ),
        migrations.AlterField(
            model_name='note',
            name='tags',
            field=models.ManyToManyField(related_name='tags', to='notty_api.Tag'),
        ),
    ]
