from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django_mptt_admin.admin import DjangoMpttAdmin
from notty_api.models import *


class CategoryAdmin(DjangoMpttAdmin):
    tree_auto_open = True


class ColorAdmin(admin.ModelAdmin):
    list_display = ('color_name', 'color_hex', )


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'userprofile'


class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Note)
admin.site.register(Media)
admin.site.register(Tag)
admin.site.register(Color, ColorAdmin)

