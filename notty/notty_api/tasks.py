from __future__ import absolute_import
from celery import shared_task
from django.core.mail import send_mail
from django.core.mail import EmailMessage

@shared_task
def add(x, y):
    return x + y


@shared_task
def heyho():
    for i in range(10):
        print 'heyho!'


@shared_task
def send():
    email = EmailMessage('Hello', 'World', to=['fazen46@gmail.com'])
    email.send()
    print 1