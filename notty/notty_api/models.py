from django.db import models
from colorful.fields import RGBColorField
from django.contrib.auth.models import User, UserManager
import mptt
from mptt.fields import TreeForeignKey
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

DEFAULT_COLOR_ID = 1


class Tag(models.Model):
    tag_name = models.CharField(max_length=30)

    def __unicode__(self):
        return u'%s' % self.tag_name


class Color(models.Model):
    color_name = models.CharField(max_length=20)
    color_hex = RGBColorField()

    def __unicode__(self):
        return u'%s' % self.color_name


class Media(models.Model):
    media_item = models.FileField(upload_to='files/', blank=True, null=True)

    def __unicode__(self):
        return u'%s' % self.media_item.name


class Category(models.Model):
    title = models.CharField(max_length=255, null=False,
                             default='title', )
    parent = TreeForeignKey('self', blank=True,
                            null=True, related_name='child')

    def __unicode__(self):
        return u'%s' % self.title


class Note(models.Model):
    title = models.CharField(max_length=15, blank=False)
    content = models.CharField(max_length=120, blank=False)
    picture = models.ImageField(upload_to='images/', blank=True, null=True)
    color = models.ForeignKey(Color, default=DEFAULT_COLOR_ID,
                              related_name='color')
    date = models.DateTimeField()

    tags = models.ManyToManyField(Tag, related_name='tags', blank=True,
                                  null=True)
    categories = models.ForeignKey(Category, related_name='categories',
                                   null=True, blank=True)

    medias = models.ManyToManyField(Media, related_name='media_files')

    def __unicode__(self):
        return u'%s %s' % (self.id, self.title)


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    avatar = models.ImageField(upload_to='avatars/', blank=True, null=True)
    notes = models.ManyToManyField(Note, related_name='notes')


@receiver(pre_delete, sender=Note)
def note_delete(sender, instance, **kwargs):
    print instance
    instance.picture.delete(False)


mptt.register(Category, )