from django.conf.urls import include, url
from django.contrib import admin
from django.views.static import serve
from main import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    url(r'^notty-api/', include('notty_api.urls', namespace='notty-api')),



    url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], serve,
        {'document_root': settings.MEDIA_ROOT}),
    url(r'^%s(?P<path>.*)$' % settings.STATIC_URL[1:],
        'django.contrib.staticfiles.views.serve', dict(insecure=True)),

    url(r'^docs/', include('rest_framework_swagger.urls')),
]
